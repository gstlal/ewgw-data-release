### PLUG IN
####################################
GSTLAL_GIT_DIR=          # gstlal git repo
FRAMES_DIR=              # path to directory with GW frames
EWGW_DATA_RELEASE_DIR=   # path to the ew-gw data release repo
WEBDIR =                 # path to web-page for results

####################################
# The GPS start time for analysis
START = 1000000000
# The GPS end time for analysis
STOP = 1002591000

# Duration
DURATION=$(shell echo $(STOP) - $(START) | bc -l)

# A user tag for the run
TAG = 38Hz
# Run number
RUN = early_warning
# A web directory for output



############################
# Template bank parameters #

# Low frequency cut off for the template bank placement
LOW_FREQUENCY_CUTOFF = 10.0
# Request the highest sample rate
SAMPLE_RATE = 2048
# Highest frequency at which to compute the metric
HIGH_FREQUENCY_CUTOFF=38
# Controls the number of templates in each SVD sub bank
NUM_SPLIT_TEMPLATES = 500
# Number of bins of chi to uniformly bin templates into
NUM_CHI_BINS = 1
# Controls the overlap from sub bank to sub bank - helps mitigate edge effects
# in the SVD.  Redundant templates will be removed
OVERLAP = 30
# The approximant that you wish to filter with for BANK_MCHIRP1 and BANK_MCHIRP2, respectively.
MCHIRP_SPLIT = 1000
APPROXIMANT1 = 0.00:$(MCHIRP_SPLIT):TaylorF2
APPROXIMANT2 = $(MCHIRP_SPLIT):1000.0:SEOBNRv4_ROM

#########################
# Triggering parameters #
#########################

# The detectors to analyze
IFOS = H1 L1 V1
# Minimum number of detecors working to use
MIN_IFOS = 2
# The GPS start time for analysis
# The number of sub banks in each SVD bank file
NUMBANKS = 2
# The control peak time for the composite detection statistic.  If set to 0 the
# statistic is disabled
PEAK = 0
# The length of autocorrelation chi-squared in sample points
AC_LENGTH = 1401
# The minimum number of samples to include in a given time slice
SAMPLES_MIN = 2048
# The maximum number of samples to include in the 256 Hz or above time slices
SAMPLES_MAX_256 = 2048

#############################
# additional options, e.g., #
#############################

#ADDITIONAL_DAG_OPTIONS = "--samples-min $(SAMPLES_MIN) --samples-max-256 $(SAMPLES_MAX_256) --blind-injections BNS-MDC1-WIDE.xml"
ADDITIONAL_DAG_OPTIONS:=--samples-min $(SAMPLES_MIN) --samples-max-256 $(SAMPLES_MAX_256)

##############
# Injections #
##############

# Change as appropriate, whitespace is important
MCHIRP_INJECTIONS := 0.5:100.0:bns_astrophysical-1000000000-1002592000.xml.gz

################
# Channel info #
################

H1_CHANNEL=FAKE
L1_CHANNEL=FAKE
V1_CHANNEL=FAKE

CHANNEL_NAMES:=--channel-name=H1=$(H1_CHANNEL) --channel-name=L1=$(L1_CHANNEL) --channel-name=V1=$(V1_CHANNEL)

###################################################################################
# Get some basic definitions.  NOTE this comes from the share directory probably. #
###################################################################################

include $(GSTLAL_GIT_DIR)/gstlal-inspiral/share/Makefile.offline_analysis_rules

BANK_CACHE_STRING:=H1=H1_split_bank.cache,L1=L1_split_bank.cache,V1=V1_split_bank.cache
BANK_CACHE_FILES:=H1_split_bank.cache L1_split_bank.cache V1_split_bank.cache
# the point of this is to build the string e.g. H1=../bank/H1_bank.cache,L1=../bank/L1_bank.cache


############
# Workflow #
############

all : dag SNR_sed
	@echo "Submit with: condor_submit_dag -maxjobs 3000 -maxidle 50 trigger_pipe.dag"
	@echo "Monitor with: tail -f trigger_pipe.dag.dagman.out | grep -v -e ULOG -e monitoring"

bns_astrophysical-1000000000-1002592000.xml.gz :
	cp $(EWGW_DATA_RELEASE_DIR)/analysis-files/injections/bns_astrophysical-1000000000-1002592000.xml.gz $@
	ligolw_no_ilwdchar -v $@

SNR_sed : dag
	sed -i 's@environment = GST_REGISTRY_UPDATE=no;@environment = "GST_REGISTRY_UPDATE=no LD_PRELOAD=$(MKLROOT)/lib/intel64/libmkl_core.so"@g' gstlal_inspiral_injection_snr.sub

# Run inspiral pipe to produce dag
dag : segments.xml.gz vetoes.xml.gz frame.cache inj_tisi.xml tisi.xml plots $(WEBDIR) $(INJECTIONS) $(BANK_CACHE_FILES) bns_astrophysical-1000000000-1002592000.xml.gz
	gstlal_inspiral_pipe \
		--data-source frames \
		--psd-fft-length 8 \
		--gps-start-time $(START) \
		--gps-end-time $(STOP) \
		--frame-cache frame.cache \
		--frame-segments-file segments.xml.gz \
		--vetoes vetoes.xml.gz \
		--frame-segments-name datasegments  \
		--control-peak-time $(PEAK) \
		--template-bank gstlal_bank_MCHIRP_CUT.xml.gz \
		--num-banks $(NUMBANKS) \
		--fir-stride 1 \
		--web-dir $(WEBDIR) \
		--time-slide-file tisi.xml \
		--inj-time-slide-file inj_tisi.xml \
		$(INJECTION_LIST) \
		--bank-cache $(BANK_CACHE_STRING) \
		--tolerance 0.9999 \
		--overlap $(OVERLAP) \
		--flow $(LOW_FREQUENCY_CUTOFF) \
		--sample-rate $(SAMPLE_RATE) \
		--fmax $(HIGH_FREQUENCY_CUTOFF) \
		$(CHANNEL_NAMES) \
		--autocorrelation-length $(AC_LENGTH) \
		$(ADDITIONAL_DAG_OPTIONS) \
		$(CONDOR_COMMANDS) \
		--dtdphi-file $(EWGW_DATA_RELEASE_DIR)/analysis-files/dTdPhi/38/inspiral_dtdphi_pdf.h5 \
		--ht-gate-threshold-linear 0.8:15.0-45.0:100.0 \
		--min-instruments $(MIN_IFOS) \
		--ranking-stat-samples 4194304 \
		--coincidence-threshold 0.010 \
		--mass-model narrow-bns
	sed -i '1s/^/JOBSTATE_LOG logs\/trigger_pipe.jobstate.log\n/' trigger_pipe.dag
	# Add --sample-rate 4096 to gstlal_reference_psd.sub arguments
	sed -i 's@--frame-cache@--sample-rate 4096 --frame-cache@' gstlal_reference_psd.sub
	# Following three lines are use to make dynamical memory requests.

%_split_bank.cache : gstlal_bank_MCHIRP_CUT.xml.gz
	mkdir -p $*_split_bank
	gstlal_bank_splitter \
		--f-low $(LOW_FREQUENCY_CUTOFF) \
		--group-by-chi $(NUM_CHI_BINS) \
		--output-path $*_split_bank \
		--approximant $(APPROXIMANT1) \
		--approximant $(APPROXIMANT2) \
		--output-cache $@ \
		--overlap $(OVERLAP) \
		--instrument $* \
		--n $(NUM_SPLIT_TEMPLATES) \
		--sort-by mchirp \
		--max-f-final $(HIGH_FREQUENCY_CUTOFF) \
		--write-svd-caches \
		--num-banks $(NUMBANKS) \
		$<

gstlal_bank_MCHIRP_CUT.xml.gz : gstlal_bank.sqlite
	cp $< gstlal_bank_MCHIRP_CUT.sqlite
	sqlite3 gstlal_bank_MCHIRP_CUT.sqlite "DELETE FROM sngl_inspiral WHERE mchirp <= 0.9 or mchirp >= 1.7;"
	ligolw_sqlite -d gstlal_bank_MCHIRP_CUT.sqlite -v -x $@
	sqlite3 $< "SELECT count(*) FROM sngl_inspiral;"
	sqlite3 gstlal_bank_MCHIRP_CUT.sqlite "SELECT count(*) FROM sngl_inspiral;"
	gstlal_inspiral_add_template_ids gstlal_bank_MCHIRP_CUT.xml.gz

gstlal_bank.sqlite : gstlal_bank.xml.gz
	ligolw_sqlite -d $@ -v $<

gstlal_bank.xml.gz :
	cp $(EWGW_DATA_RELEASE_DIR)/analysis-files/bank/ew_bank.xml.gz $@
	ligolw_no_ilwdchar -v $@

# Produce time slides file
tisi.xml : inj_tisi.xml
	lalapps_gen_timeslides --instrument=H1=0:0:0 --instrument=L1=25.13274:25.13274:25.13274 --instrument=V1=12.345:12.345:12.345 bg_tisi.xml
	ligolw_add --output $@ bg_tisi.xml $<

# Produce injection time slide file
inj_tisi.xml :
	lalapps_gen_timeslides --instrument=H1=0:0:0 --instrument=L1=0:0:0 --instrument=V1=0:0:0 $@

# Produce veto file
vetoes.xml.gz:
	echo 999999997 999999998 > vetoes.txt
	ligolw_segments  --insert-from-segwizard=H1=vetoes.txt --output H1_vetoes.xml.gz --name=vetoes
	ligolw_segments  --insert-from-segwizard=L1=vetoes.txt --output L1_vetoes.xml.gz --name=vetoes
	ligolw_segments  --insert-from-segwizard=V1=vetoes.txt --output V1_vetoes.xml.gz --name=vetoes
	ligolw_add --output $@ H1_vetoes.xml.gz L1_vetoes.xml.gz V1_vetoes.xml.gz
	gzip $@

# Produce segments file
segments.xml.gz:
	echo $(START) $(STOP) > segs.txt
	ligolw_segments --insert-from-segwizard=H1=segs.txt --output H1_segs.xml.gz --name=datasegments
	ligolw_segments --insert-from-segwizard=L1=segs.txt --output L1_segs.xml.gz --name=datasegments
	ligolw_segments --insert-from-segwizard=V1=segs.txt --output V1_segs.xml.gz --name=datasegments
	ligolw_add --output $@ H1_segs.xml.gz L1_segs.xml.gz V1_segs.xml.gz

# Produce frame cache files
frame.cache :
	cp $(FRAMES_DIR)/frame.cache .

# Make webpage directory and copy files across
$(WEBDIR) : $(MAKEFILE_LIST)
	mkdir -p $(WEBDIR)/OPEN-BOX
	cp $(MAKEFILE_LIST) $@

# Makes local plots directory
plots :
	mkdir plots

clean :
	-rm -rvf *.sub *.dag* *.cache *.sh logs *.sqlite plots *.html Images *.css *.js
	-rm -rvf lalapps_run_sqlite/ ligolw_* gstlal_*
	-rm -vf segments.xml.gz tisi.xml H1-*.xml H1_*.xml L1-*.xml L1_*xml V1-*.xml V1_*xml ?_injections.xml ????-*_split_bank-*.xml vetoes.xml.gz
	-rm -vf *marginalized*.xml.gz *-ALL_LLOID*.xml.gz
	-rm -vf tisi0.xml tisi1.xml
	-rm -rf *_split_bank*
	-rm -rf nogaps.xml segdb.xml
	-rm -rf bank_aligned_spin.xml.gz
	-rm -rf CAT1*.xml.gz
	-rm bg_tisi.xml H1_segs.xml.gz inj_tisi.xml L1_vetoes.xml.gz segs.txt V1_vetoes.xml.gz bns_astrophysical-1000000000-1000500000.xml.gz H1_vetoes.xml.gz L1_segs.xml.gz V1_segs.xml.gz  vetoes.txt

