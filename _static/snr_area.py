# SNR vs Area, Distance Plots

from astropy.io import ascii
import matplotlib.pyplot as plt

data = ascii.read('_static/combinedtable.txt', data_start=43)

def snr_area_plot():
    plt.scatter(data['col23'], data['col19'])
    plt.xlabel('90% area in Sq.Deg.')
    plt.ylabel('SNR')
    plt.show()
snr_area_plot()

def snr_dist_plot():
    plt.scatter(data['col9'], data['col19'], c=data['col6'])
    plt.colorbar()
    plt.xlabel('Distance in Mpc')
    plt.ylabel('SNR')
    plt.show()
snr_dist_plot()
