.. _simtable:

Injection Recovery
==========================

Here we present the parameters of the recovered injections at various
frequencies. The table can be sorted by the table headers. The gpstimes for
various injections can be clicked on to view an animated gif showing the
evolution of skymap at different times before merger. This table contains a
subset of the injections (those that have their gpstimes between 1000000000 and
1000100000). But the full table can be downloaded here:
:download:`Download table (csv) <_static/combinedlist.csv>` 

All the animated skymap gifs can be downloaded here:
`GIFS`_

+---------+----------------+--------------------+---------------------------------------+
| Simulated and Detected BNS signals                                                    |
+=========+================+====================+=======================================+
| Column  |   Label        |   Units            |   Explanations                        |
+---------+----------------+--------------------+---------------------------------------+
| |  1    | | gpstime      | |  sec             | | GPS time of simulated signal        |
| |  2    | | mass1        | |  solMass         | | Mass of binary component 1          |
| |  3    | | mass2        | |  solMass         | | Mass of binary component 2          |
| |  4    | | distance     | |  Mpc             | | Distance                            |
| |  5    | | snr-1024     | |  ---             | | Network SNR at 1024 Hz              |
| |  6    | | snr-56       | |  ---             | | Network SNR at 56 Hz                |
| |  7    | | snr-49       | |  ---             | | Network SNR at 49 Hz                |
| |  8    | | snr-38       | |  ---             | | Network SNR at 38 Hz                |
| |  9    | | snr-32       | |  ---             | | Network SNR at 32 Hz                |
| |  10   | | snr-29       | |  ---             | | Network SNR at 29 Hz                |
| |  11   | | area-1024    | |  deg2            | | 90% area at 1024 Hz                 |
| |  12   | | area-56      | |  deg2            | | 90% area at 56 Hz                   |
| |  13   | | area-49      | |  deg2            | | 90% area at 49 Hz                   |
| |  14   | | area-38      | |  deg2            | | 90% area at 38 Hz                   |
| |  15   | | area-32      | |  deg2            | | 90% area at 32 Hz                   |
| |  16   | | area-29      | |  deg2            | | 90% area at 29 Hz                   |
+---------+----------------+--------------------+---------------------------------------+

.. _GIFS: https://git.ligo.org/gstlal/ewgw-data-release/-/tree/master/gif
.. raw:: html

    <iframe src="_static/recovered_injections.html" height="900px" width="100%" frameborder="0" allowfullscreen></iframe> 
