Pre-merger detection of binary neutron star mergers using `GSTLAL`_
===================================================================
This web page provides details of the data used in the paper "`An early warning
system for electromagnetic follow-up of gravitational-wave events`_." The paper
describes the pre-merger detection of the binary neutron star (BNS) systems and
the prospects for electromagnetic follow-up considering a network of three
ground-based gravitational-wave (GW) detectors: `Advanced LIGO`_ (Hanford and
Livingston), and `Advanced Virgo`_ operating at design sensitivities.

.. image:: _static/gif/1000020250.gif
  :width:  500

*Evolution of sky localization from GW detection with time, the final (smallest) localization is at merger.*

About 7% (respectively, 49%) of the total detectable BNS mergers will
be detected 60 s (10 s) before the merger.  About 2% of the total detectable
BNS mergers will be detected before merger and localized to within 100 deg\ :sup:`2` (90% credible interval).  


Here we provide in form of tables (CSV and JSON formats), the data (:ref:`data`) that were
used to inform results in the paper. In addition, we also provide links to and
description of the full raw GW data (:ref:`data-full`) set which can be used by both professional
astronomers and science enthusiasts for further studies. In particular, we
provide the posterior probability maps resulting from Bayesian parameter
estimation at various times before merger for a number of simulated BNS sources
that we expect to detect. These should be useful to prepare EM-facilities for
optimal follow-up.


Content
-------

.. toctree::
    
    data
    data-full


The gpstimes for various simulated signals in the section :ref:`simtable` can be
clicked on to view an animated gif showing the evolution of skymap at different
times before merger. All the
animated skymap gifs can be downloaded here:
`GIFS`_

.. _GIFS: https://git.ligo.org/gstlal/ewgw-data-release/-/tree/master/gif

Appendix
--------

.. toctree::
   :hidden:



* :ref:`search`
* `Contact`_
 

.. _`Contact`: _static/contact.html
.. _`GstLAL`: https://lscsoft.docs.ligo.org/gstlal/
.. _`GCN`: https://gcn.gsfc.nasa.gov/gcn3_circulars.html
.. _`Advanced LIGO`: https://ligo.caltech.edu
.. _`Advanced Virgo`: http://www.virgo-gw.eu
.. _`BAYESTAR`: https://lscsoft.docs.ligo.org/ligo.skymap/ligo/skymap/bayestar.html
.. _`HEALPix`: https://healpix.sourceforge.io
.. _`FITS`: https://fits.gsfc.nasa.gov/fits_documentation.html
.. _`ASCII`: http://www.asciitable.com
.. _`AstroPy`: https://www.astropy.org
.. _`LIGO_LW XML`: https://gwpy.github.io/docs/latest/table/io.html#gwpy-table-io-ligolw
.. _`HealPy`: https://healpy.readthedocs.io/en/latest/
.. _`An early warning system for electromagnetic follow-up of gravitational-wave events`: https://arxiv.org/abs/2008.04288
.. _`First Two Years Data`: https://arxiv.org/abs/1404.5623 
.. _`snr-time-series`: ftp://data1.commons.psu.edu/pub/commons/physics/jfs6271/

