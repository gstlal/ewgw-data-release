.. _injection:

Injected parameters
=============================

Here we present various parameters of all the injections that were made (259,200 out of 1,918,947). The injections that have an SNR smaller than 3 in all detectors (considered non-detectable), are not made to save on computational costs.

+---------+-----------------+--------------------+----------------------------------------+
| Simulated and Detected BNS signals                                                      |
+=========+=================+====================+========================================+
| Column  |  Label          |  Units             |   Explanations                         |
+---------+-----------------+--------------------+----------------------------------------+
| |  1    | | gpstime       | |  sec             | | GPS time of simulated signal         |
| |  2    | | RAdeg         | |  deg             | | Right ascension                      |
| |  3    | | DECdeg        | |  deg             | | Declination                          |
| |  4    | | inclination   | |  deg             | | Binary orbital inclination angle     |
| |  5    | | polarization  | |  deg             | | Polarization angle                   |
| |  6    | | coa-phase     | |  deg             | | Orbital phase at coalescence         |
| |  7    | | distance      | |  Mpc             | | Luminosity distance                  |
| |  8    | | mass1         | |  solMass         | | Mass of binary component 1           |
| |  9    | | mass2         | |  solMass         | | Mass of binary component 2           |
+---------+-----------------+--------------------+----------------------------------------+
Note (1): Searched area is the area searched in the sky before arriving at the true
          location of the signal

.. raw:: html

    <iframe src="_static/injection.html" height="900px" width="100%" frameborder="0" allowfullscreen></iframe> 

