.. _data:

Summary of results and data
===========================

.. list-table:: Results
   :widths: 20 20 20 20

   * - Injected and recovered parameters of all detected events
     - :ref:`simtable`
     - :download:`Download table (csv) <_static/combinedlist.csv>`
     - :download:`Download table (json) <_static/recovered.json>`
   * - 29 Hz
     - :ref:`29`
     - :download:`Download table (csv) for 29 Hz  <_static/29.csv>`
     - :download:`Download table (json) for 29 Hz  <_static/29.json>`
   * - 32 Hz 
     - :ref:`32`
     - :download:`Download table (csv) for 32 Hz  <_static/32.csv>`
     - :download:`Download table (json) for 32 Hz  <_static/32.json>`
   * - 38 Hz 
     - :ref:`38`
     - :download:`Download table (csv) for 38 Hz  <_static/38.csv>`
     - :download:`Download table (json) for 38 Hz  <_static/38.json>`
   * - 49 Hz 
     - :ref:`49`
     - :download:`Download table (csv) for 49 Hz  <_static/49.csv>`
     - :download:`Download table (json) for 49 Hz  <_static/49.json>`
   * - 56 Hz 
     - :ref:`56`
     - :download:`Download table (csv) for 56 Hz  <_static/56.csv>`
     - :download:`Download table (json) for 56 Hz  <_static/56.json>`
   * - 1024 Hz 
     - :ref:`1024`
     - :download:`Download table (csv) for 1024 Hz  <_static/1024.csv>`
     - :download:`Download table (json) for 1024 Hz  <_static/1024.json>`

.. list-table:: Injection Parameters
   :widths: 20 20 20 20

   * - Full simulated injection parameters
     - :ref:`injection`
     - :download:`Download table (csv) injections  <_static/injection.csv>`
     - :download:`Download table (json) injections  <_static/injection.json>`

We inject 1,918,947 simulated BNS signals (injections) into the simulated data.
Both source frame component masses are drawn from a Gaussian population defined
between 1.0 :math:`M_\odot` < m\ :sub:`1`, m\ :sub:`2` < 2.0 :math:`M_\odot`
with mean mass of 1.33 :math:`M_\odot` and standard deviation of 0.09
:math:`M_\odot`. These non-spining BNS sources are uniformly distributed in a
comoving volume up to a redshift of z =0.2. We use the offline early-warning
configuration of the `GstLAL`_ pipeline to recover the injected waveforms from
the data. Candidates with signal-to-noise ratios below 4.0 are discarded to
reduce the volume of triggers. We use six search configurations: the search of
the signal in the data starts at 10 Hz and ends at 29 Hz, 32 Hz, 38 Hz, 49 Hz,
56 Hz, and 1024 Hz to analyze signal recovery at (approximately) 60 seconds, 45
seconds, 30 seconds, 15 seconds, 10 seconds, and 0 seconds before merger. We
consider injections that are recovered with a false-alarm-rate (FAR) of less
than a month as detected. We localize the detected candidates using a rapid
Bayesian algorith, `BAYESTAR`_ for all the final frequencies.

We present here various parameters (luminosity distance, SNR, sky localization)
of the recovered injections at verious frequencies in form of tables.
:ref:`simtable` provides a complete view of the injection at all frequencies.
One can also view animated gifs showing the evolution of skymap for each
injection at various times before merger. We also present the results for each
frequencies separately.


.. toctree::

   simtable
   29
   32
   38
   49
   56
   1024
   injection


.. _`GstLAL`: https://lscsoft.docs.ligo.org/gstlal/
.. _`GCN`: https://gcn.gsfc.nasa.gov/gcn3_circulars.html
.. _`Advanced LIGO`: https://ligo.caltech.edu
.. _`Advanced Virgo`: http://www.virgo-gw.eu
.. _`BAYESTAR`: https://lscsoft.docs.ligo.org/ligo.skymap/ligo/skymap/bayestar.html
.. _`HEALPix`: https://healpix.sourceforge.io
.. _`FITS`: https://fits.gsfc.nasa.gov/fits_documentation.html
.. _`ASCII`: http://www.asciitable.com
.. _`AstroPy`: https://www.astropy.org
.. _`LIGO_LW XML`: https://gwpy.github.io/docs/latest/table/io.html#gwpy-table-io-ligolw
.. _`HealPy`: https://healpy.readthedocs.io/en/latest/
.. _`An early warning system for electromagnetic follow-up of gravitational-wave events`: https://arxiv.org/abs/2008.04288
.. _`First Two Years Data`: https://arxiv.org/abs/1404.5623 
.. _`snr-time-series`: ftp://data1.commons.psu.edu/pub/commons/physics/jfs6271/
